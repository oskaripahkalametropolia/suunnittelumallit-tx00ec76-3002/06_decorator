public class EncryptedFileWrite extends FileReadWriteDecorator {

    public EncryptedFileWrite(FileReadWrite component) {
        super(component);
    }

    @Override
    public String read(String path){
        String data = super.read(path);
        data = Base64.decode(data);
        return data;
    }

    @Override
    public void write(String path, String data){
        data = Base64.encode(data);
        super.write(path, data);
    }
}
