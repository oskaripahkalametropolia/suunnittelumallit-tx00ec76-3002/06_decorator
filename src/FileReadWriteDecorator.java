public abstract class FileReadWriteDecorator implements FileReadWrite {
    private FileReadWrite component;
    public FileReadWriteDecorator(FileReadWrite component){
        this.component = component;
    }

    @Override
    public void write(String path, String data) {
        component.write(path, data);
    }

    @Override
    public String read(String path){
        return component.read(path);
    }
}
