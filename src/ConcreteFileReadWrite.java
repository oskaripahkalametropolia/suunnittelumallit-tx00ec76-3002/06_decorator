import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ConcreteFileReadWrite implements FileReadWrite {

    @Override
    public String read(String path) {
        StringBuilder content = new StringBuilder();
        try (Scanner fileReader = new Scanner(new File(path))) {
            while (fileReader.hasNextLine()){
                content.append(fileReader.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    @Override
    public void write(String path, String data) {
        try {
            File file = new File(path);
            if (!file.exists()){
                if (file.createNewFile()){
                    throw new IOException("Failed to create file");
                }
            }
            try (FileWriter fileWriter = new FileWriter(path)){
                fileWriter.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
