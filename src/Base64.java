import java.util.Arrays;

public class Base64 {
    private final static Character[] alphabet = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G',
            'H', 'I', 'J', 'K', 'L', 'M', 'N',
            'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g',
            'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '+', '/'//safe chars '-', '_'
    };

    static String encode(String input, Boolean safe){
        String output = encode(input);
        if (safe){
            output = output.replace('+', '-');
            output = output.replace('/', '_');
        }
        return output;
    }

    static String encode(String input){
        StringBuilder bits = new StringBuilder(
                input.chars()
                .mapToObj(Integer::toBinaryString)
                .reduce("", (a,b) -> a + "%8s".formatted(b).replace(' ','0'))
        );

        if (bits.length() % 8 != 0) {
            int count = 8 - (bits.length() % 8);
            bits.insert(0, "0".repeat(count));
        }

        int pad = 0;
        if (bits.length() % 24 == 8) pad = 2;
        if (bits.length() % 24 == 16) pad = 1;

        bits.append("0".repeat(pad * 2));

        StringBuilder output = new StringBuilder();

        for (int i = 0; i < bits.length(); i+=6) {
            String indexBits = bits.substring(i, i+6);
            int index = 0;
            for (int bit = 0; bit < 6; bit++){
                if(indexBits.charAt(bit) == '1'){
                    index |= (32 >> bit);
                }
            }
            output.append(alphabet[index]);
        }

        output.append("=".repeat(pad));

        return output.toString();
    }

    static String decode(String input, Boolean safe){
        if (safe){
            input = input.replace('+', '-');
            input = input.replace('/', '_');
        }
        return decode(input);
    }

    static String decode(String input){
        input = input.replace("\n","");
        input = input.replace("\r","");

        char[] chars = input.toCharArray();

        int pad = 0;

        int[] indices = new int[chars.length];

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != '='){
                indices[i] = Arrays.asList(alphabet).indexOf(chars[i]);
            } else {
                pad++;
            }
        }

        StringBuilder bits = new StringBuilder();
        for (int i = 0; i < indices.length - pad; i++) {
            String binary = Integer.toBinaryString(indices[i]);
            bits.append("0".repeat(6 - binary.length()));
            bits.append(binary);
        }

        StringBuilder output = new StringBuilder();

        for (int i = 0; i < bits.length() - pad * 2; i+=8) {
            output.append((char)Integer.parseInt(bits.substring(i,i+8),2));
        }

        return output.toString();
    }
}
