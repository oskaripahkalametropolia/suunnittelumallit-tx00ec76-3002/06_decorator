import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FileReadWrite plain = new ConcreteFileReadWrite();
        FileReadWriteDecorator encrypted = new EncryptedFileWrite(plain);
        FileReadWriteDecorator megaencrypted = new EncryptedFileWrite(encrypted);
        Scanner scanner = new Scanner(System.in);

        String path = "lipsum.txt";
        String data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

        System.out.println("\nkirjoita salaamatta\n");
        plain.write(path, data);
        System.out.println("lue purkamatta");
        System.out.println(plain.read(path));

        scanner.nextLine();
        System.out.println("kirjoita salaamalla\n");
        encrypted.write(path, data);
        System.out.println("lue purkamatta");
        System.out.println(plain.read(path));

        scanner.nextLine();
        System.out.println("lue purkamalla");
        System.out.println(encrypted.read(path));

        scanner.nextLine();
        System.out.println("kirjoita salaamalla kahdesti\n");
        megaencrypted.write(path, data);
        System.out.println("lue purkamatta");
        System.out.println(plain.read(path));

        scanner.nextLine();
        System.out.println("lue purkamalla kahdesti");
        System.out.println(megaencrypted.read(path));

        scanner.close();
    }
}
