public interface FileReadWrite {
    /**
     * Read from a file
     * @param path file path
     * @return file content
     */
    String read(String path);

    /**
     * Write to a file
     * @param path file path
     * @param data content to write
     */
    void write(String path, String data);
}
